package com.prototype.src.game.tests;

import org.junit.Assert;
import org.junit.Test;

import com.prototype.src.game.math.MathHelper;
import com.prototype.src.game.math.Point;
import com.prototype.src.game.math.Vector;

public class MathTests {
	@Test
	public void getVectorLenghtTest() {
		Point p1 = new Point(0, 0);
		Point p2 = new Point(0, 4);
		Vector vector = new Vector(p1, p2);
		double result = vector.getLenght();
		Assert.assertEquals(4, result, 0);
	}
	@Test
	public void getStepsTest() {
		Point p1 = new Point(0, 0);
		Point p2 = new Point(0, 110);
		Vector vector = new Vector(p1, p2);
		double result = MathHelper.getSteps(vector, 10);
		Assert.assertEquals(11, result, 0);
		p2 = new Point(40, 30);
		vector = new Vector(p1, p2);
		result = MathHelper.getSteps(vector, 10);
		Assert.assertEquals(5, result, 0);
	}
	
	@Test
	public void getNextCoordTest() {
		Point p1 = new Point(0, 0);
		Point p2 = new Point(10, 10);
		double x = MathHelper.getNextCoord(p1.getX(), p2.getX(), 10);
		double y = MathHelper.getNextCoord(p1.getY(), p2.getY(), 10);
		Assert.assertEquals(1, x, 0);
		Assert.assertEquals(1, y, 0);
		p2 = new Point(2, 4);
		x = MathHelper.getNextCoord(p1.getX(), p2.getX(), 2);
		y = MathHelper.getNextCoord(p1.getY(), p2.getY(), 2);
		Assert.assertEquals(1, x, 0);
		Assert.assertEquals(2, y, 0);
		p2 = new Point(0, 10);
		x = MathHelper.getNextCoord(p1.getX(), p2.getX(), 2);
		y = MathHelper.getNextCoord(p1.getY(), p2.getY(), 2);
		Assert.assertEquals(0, x, 0);
		Assert.assertEquals(5, y, 0);
		p2 = new Point(0, 0);
		x = MathHelper.getNextCoord(p1.getX(), p2.getX(), 70);
		y = MathHelper.getNextCoord(p1.getY(), p2.getY(), 70);
		Assert.assertEquals(0, x, 0);
		Assert.assertEquals(0, y, 0);
	}
}