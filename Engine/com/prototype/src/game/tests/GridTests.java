package com.prototype.src.game.tests;

import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import org.junit.Assert;
import org.junit.Test;

import com.prototype.src.game.Game;
import com.prototype.src.game.Grid;

public class GridTests {
	@Test
	public void setupSettingsTest() throws Exception {
		Grid grid1 = new Grid();
		grid1.setupSettings(32, 32);
		BufferedImage grid1Image = getGridImage(grid1);
		Grid grid2 = new Grid();
		grid2.setupSettings(64, 64);
		BufferedImage grid2Image = getGridImage(grid2);
		Assert.assertFalse(equalsGrids(grid1Image, grid2Image));
		grid1.setupSettings(64, 64);
		Assert.assertTrue(equalsGrids(grid1Image, grid2Image));
	}
	
	BufferedImage getGridImage(Grid grid) throws Exception {
		Field imageField = grid.getClass().getSuperclass().getDeclaredField("image");
		imageField.setAccessible(true);
		return (BufferedImage)imageField.get(grid);
	}
	
	boolean equalsGrids(BufferedImage grid1, BufferedImage grid2) {
		for(int x = 0; x < Game.Width; x++)
			if(grid1.getRGB(x, 0) != grid2.getRGB(x, 0))
				return false;
		for(int y = 0; y < Game.Height; y++)
			if(grid1.getRGB(0, y) != grid2.getRGB(0, y))
				return false;
		return true;
	}
}