package com.prototype.src.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public abstract class RenderedObject {
	protected BufferedImage image;
	
	public abstract void render(Graphics graphics);
}