package com.prototype.src.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.prototype.src.game.math.MathHelper;
import com.prototype.src.game.math.Point;
import com.prototype.src.game.math.Vector;

public class Bullet extends GameObject {
	public Bullet(Point point, Point end, Game game) {
		super(point, 4, 4);
		// GAME IS NATIVE (GAME FOR SPRITES)
		/////////////////////////////////////
		// LOGIC FOR GET PLAYER SPRITE
		image = new BufferedImage(8, 8, BufferedImage.TYPE_INT_ARGB);
		Graphics gr = image.createGraphics();
		gr.setColor(Color.WHITE);
		gr.fillOval(0, 0, 8, 8);
		gr.drawImage(image, 0, 0, null);
		///////////////////////
		this.end = end;
	}
	
	/////////////////////TEMP
	private double tx = 0;
	private double ty = 0;
	//////////////////
	private Point end;
	
	public void tick() {
		Vector vector = Vector.Empty;
		if(tx == 0 && ty == 0)
			vector = new Vector(point, new Point(end));
		if(tx == 0) {
			tx = MathHelper.getNextCoord(point.getX(), end.getX(), MathHelper.getSteps(vector, 15));
		}
		if(ty == 0) {
			ty = MathHelper.getNextCoord(point.getY(), end.getY(), MathHelper.getSteps(vector, 15));
		}
		point.setX(this.getPoint().getX() + tx);
		point.setY(this.getPoint().getY() + ty);
	}
}