package com.prototype.src.game;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseInput implements MouseListener {
	public MouseInput(Game game) {
		this.game = game;
	}
	
	Game game;
	
	public void mouseClicked(MouseEvent arg0) { }
	public void mouseEntered(MouseEvent arg0) { }
	public void mouseExited(MouseEvent arg0) { }
	public void mousePressed(MouseEvent arg0) {
		game.mousePressed(arg0);
	}
	public void mouseReleased(MouseEvent arg0) {
		game.mouseReleased(arg0);
	}
}