package com.prototype.src.game;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MouseMotionInput implements MouseMotionListener {
	public MouseMotionInput(Game game) {
		this.game = game;
	}
	
	private Game game;
	
	public void mouseDragged(MouseEvent arg0) {
		game.mouseMoved(arg0);
	}
	public void mouseMoved(MouseEvent arg0) {
		game.mouseMoved(arg0);
	}
}