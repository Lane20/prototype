package com.prototype.src.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.prototype.src.game.math.Point;

public class Crosshair extends GameObject {
	public Crosshair(Point point, Game game) {
		super(point, 8, 8);
		// GAME IS NATIVE (GAME FOR SPRITES)
		/////////////////////////////////////
		// LOGIC FOR GET PLAYER SPRITE
		image = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		Graphics gr = image.createGraphics();
		gr.setColor(Color.WHITE);
		gr.fillOval(0, 0, 16, 16);
		gr.drawImage(image, 0, 0, null);
		///////////////////////
	}
}