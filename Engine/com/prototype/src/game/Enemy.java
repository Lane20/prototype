package com.prototype.src.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import com.prototype.src.game.math.Point;
import com.prototype.src.game.ai.pathfinding.*;
import com.prototype.src.game.debug.DebugHelper;

public class Enemy extends GameObject {
	public Enemy(Point point, Game game, double offsetX, double offsetY, /*TEMP*/Player p) {
		super(point, offsetX, offsetY);
		image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics gr = image.createGraphics();
		gr.setColor(Color.PINK);
		gr.fillRect(0, 0, 32, 32);;
		gr.drawImage(image, 0, 0, null);
		this.p = p;
		g = game;
	}
	
	/////TEMP
	private Player p;
	private Game g;
	///
	public LinkedList<Node> path;
	private boolean shooting = true;
	//To weapon
	private int shotsPerSecond = 7;
	private long timePerOneShoot = 1000 / shotsPerSecond;
	private long nextTime = System.currentTimeMillis() - timePerOneShoot;
	//
	
	public void tick() {
		// Here must be code A* algorithm
		PathFinder.FindPath(getPoint(), p.point, this);
		if(path.size() > 0) {
			point.setX(path.get(0).gridX * PathFinder.XPixels);
			point.setY(path.get(0).gridY * PathFinder.YPixels);
		}
		if(shooting) {
			if(nextTime - System.currentTimeMillis() <= 0) {
				nextTime = System.currentTimeMillis() + timePerOneShoot;
				this.shoot();
			}
		}
	}
	
	@Override
	public void render(Graphics graphics) {
		if(path != null) {
			BufferedImage img = new BufferedImage(8, 8, BufferedImage.TYPE_INT_ARGB);
			Graphics gr = img.createGraphics();
			gr.setColor(Color.RED);
			gr.fillRect(0, 0, 5, 5);
			gr.drawImage(img, 0, 0, null);
			for(Node node : path) {
				graphics.drawImage(img, node.gridX * PathFinder.XPixels, node.gridY * PathFinder.YPixels, null);
			}
		}
		DebugHelper.showCoordsObject(graphics, this.point, this.offsetX, this.offsetY);
        graphics.drawImage(image, (int)(point.getX() - offsetX), (int)(point.getY() - offsetY), null);
	}
	
	public void shoot() {
		if(point.getX() != p.point.getX() || point.getY() != p.point.getY()) {
			Bullet bullet = new Bullet(new Point(point), new Point(p.point), g);
			g.getController().addBullet(bullet);
		}
	}
}