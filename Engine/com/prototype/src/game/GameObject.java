package com.prototype.src.game;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.prototype.src.game.debug.DebugHelper;
import com.prototype.src.game.math.Point;

public class GameObject extends RenderedObject {
	public GameObject(Point point, double offsetX, double offsetY) {
		this.point = point;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
	
	protected Point point;
	protected double offsetX;
	protected double offsetY;
	
	public Point getPoint() { return this.point; }
	public void setPoint(Point point) { this.point = point; }
	public void render(Graphics graphics) {
		DebugHelper.showCoordsObject(graphics, this.point, this.offsetX, this.offsetY);
        graphics.drawImage(image, (int)(point.getX() - offsetX), (int)(point.getY() - offsetY), null);
	}
	public Rectangle getBounds() {
		return new Rectangle((int)point.getX(), (int)point.getY(), (int)offsetX * 2, (int)offsetY * 2);
	}
}