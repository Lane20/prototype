package com.prototype.src.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import com.prototype.src.game.math.MathHelper;
import com.prototype.src.game.math.Point;

public class Player extends GameObject {
	public Player(Point point, Game g, Crosshair crosshair) {
		super(point, 16, 16);
		this.shooting = false;
		// GAME IS NATIVE (GAME FOR SPRITES)
		/////////////////////////////////////
		// LOGIC FOR GET PLAYER SPRITE
		image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_RGB);
		Graphics gr = image.createGraphics();
		gr.setColor(Color.WHITE);
		gr.fillRect(0, 0, 32, 32);
		gr.drawImage(image, 0, 0, null);
		///////////////////////
		//////TEMP
		this.game = g;
		////
		this.crosshair = crosshair;
	}
	
	private double velX = 0;
	private double velY = 0;
	private boolean shooting;
	private Crosshair crosshair;
	/////TEMP
	private Game game;
	//////
	//To weapon
	private int shotsPerSecond = 7;
	private long timePerOneShoot = 1000 / shotsPerSecond;
	private long nextTime = System.currentTimeMillis() - timePerOneShoot;
	//
	
	public void tick() {
		if(this.shooting) {
			if(nextTime - System.currentTimeMillis() <= 0) {
				nextTime = System.currentTimeMillis() + timePerOneShoot;
				this.shoot();
			}
		}
		this.getPoint().setX(this.getPoint().getX() + velX);
		if(this.getPoint().getX() <= 0)
			this.getPoint().setX(0);
		else if(this.getPoint().getX() >= Game.Width - 20)
			this.getPoint().setX(Game.Width - 20);
		this.getPoint().setY(this.getPoint().getY() + velY);
		if(this.getPoint().getY() <= 0)
			this.getPoint().setY(0);
		else if(this.getPoint().getY() >= Game.Height - 20)
			this.getPoint().setY(Game.Height - 20);
	}
	
	public void setVelX(double velX) {
		this.velX = velX;
	}
	
	public void setVelY(double velY) {
		this.velY = velY;
	}
	
	public void setShooting(boolean shooting) {
		this.shooting = shooting;
	}
	
	public void shoot() {
		if(this.getPoint().getX() != this.crosshair.getPoint().getX() || this.getPoint().getY() != this.crosshair.getPoint().getY()) {
			Bullet bullet = new Bullet(new Point(point), crosshair.point, game);
			game.getController().addBullet(bullet);
		}
	}
	
	public Crosshair getCrosshair() { return this.crosshair; }
	
	@Override
	public void render(Graphics graphics) {
		AffineTransform affineTransform = new AffineTransform();
		affineTransform.translate(this.getPoint().getX(), this.getPoint().getY());
		affineTransform.rotate(MathHelper.getRotateTheta(point, crosshair.point));
		affineTransform.translate(-image.getWidth() / 2, -image.getHeight() / 2);
        Graphics2D graphics2d = (Graphics2D) graphics;
        graphics2d.drawImage(image, affineTransform, null);
	}
}