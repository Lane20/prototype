package com.prototype.src.game.math;

public class Point extends Coords {
	public Point(Point point) {
		this(point.getX(), point.getY());
	}
	
	public Point(double x, double y) {
		super(x, y);
	}
	
	public static Point Null = new Point(0, 0);
	
	public Point Clone() { return new Point(this.getX(), this.getY()); }
}