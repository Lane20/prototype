package com.prototype.src.game.math;

public class Vector extends Coords {
	public Vector(Vector vector) {
		this(vector.getA().Clone(), vector.getB().Clone());
	}
	
	public Vector(Point a, Point b) {
		super(b.getX() - a.getX(), b.getY() - a.getY());
		this.a = a;
		this.b = b;
		this.lenght = Math.sqrt(this.getX() * this.getX() + this.getY() * this.getY());
	}
	
	public static Vector Empty = new Vector(Point.Null, Point.Null);
	
	private Point a;
	private Point b;
	private double lenght;
	
	public Point getA() { return this.a; }
	public Point getB() { return this.b; }
	public double getLenght() { return this.lenght; }
}