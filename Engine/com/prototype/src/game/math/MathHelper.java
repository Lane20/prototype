package com.prototype.src.game.math;

public class MathHelper {
	public static double getNextCoord(double a, double b, double steps) {
        return a != b ? ((b - a) / steps) : 0;
	}
	
	public static double getSteps(Vector vector, double speed) {
		return vector.getLenght() / speed;
	}
	
	public static double getScalarProduct(Vector a, Vector b) {
		return a.getX() * b.getX() + a.getY() * b.getY();
	}
	
	public static double getAngleBetweenVectors(Vector a, Vector b) {
		return Math.acos(getScalarProduct(a, b) / (a.getLenght() * b.getLenght()));
	}
	
	public static double getRotateTheta(Point a, Point b) {
		return Math.atan2(a.getY() - b.getY(), a.getX() - b.getX()) - Math.PI / 2;
	}
}