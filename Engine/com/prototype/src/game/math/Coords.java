package com.prototype.src.game.math;

public class Coords {
	public Coords(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	private double x;
	private double y;
	
	public double getX() { return this.x; }
	public void setX(double x) { this.x = x; }
	public double getY() { return this.y; }
	public void setY(double y) { this.y = y; }
}