package com.prototype.src.game.ai.pathfinding;

public interface IHeapItem extends Comparable<Object> {
	int getHeapIndex();
	void setHeapIndex(int index);
}