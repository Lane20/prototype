package com.prototype.src.game.ai.pathfinding;

import java.util.LinkedList;

import com.prototype.src.game.Enemy;
import com.prototype.src.game.Game;
import com.prototype.src.game.math.Point;

public class PathFinder {
	public static int XPixels = 5;
	public static int YPixels = 5;
	
	static Node[][] NodeArray;
	//NEED REFACTORING
	public static void InitializeNodeArray() {
		NodeArray = new Node[Game.Width / XPixels][Game.Height / YPixels];
		for(int i = 0; i < Game.Width / XPixels; i++) {
			NodeArray[i] = new Node[Game.Height / YPixels];
			for(int j = 0; j < Game.Height / YPixels; j++)
				NodeArray[i][j] = new Node(i, j); 
		}
	}
	
	public static LinkedList<Node> getNeighbours(Node node) {
		LinkedList<Node> neighbours = new LinkedList<Node>();
		for(int i = -1; i < 2; i++) {
			for(int j = -1; j < 2; j++) {
				if(i == 0 && j == 0)
					continue;
				
				int checkX = node.gridX + i;
				int checkY = node.gridY + j;
				
				if(checkX >= 0 && checkX < Game.Width / XPixels && checkY >= 0 && checkY < Game.Height / YPixels)
					neighbours.add(NodeArray[checkX][checkY]);
			}
		}
		
		return neighbours;
	}
	
	public static void FindPath(Point start, Point targer, Enemy obj) {
		Node startNode = NodeArray[(int)(start.getX() / XPixels)][(int)(start.getY() / YPixels)];
		Node targetNode = NodeArray[(int)(targer.getX() / XPixels)][(int)(targer.getY() / YPixels)];
		
		Heap openSet = new Heap(Game.Width * Game.Height);
		LinkedList<Node> closedSet = new LinkedList<Node>();
		openSet.add(startNode);
		
		while(openSet.getCount() > 0) {
			Node currentNode = (Node)openSet.removeFirst();
			closedSet.add(currentNode);
			
			if(currentNode == targetNode) {
				obj.path = RetracePath(startNode, targetNode);
				return;
			}
			
			for(Node neighbour : getNeighbours(currentNode)) {
				//WALK LOGIC?
				if(closedSet.contains(neighbour))
					continue;
				
				int newMovementCostToNeighbour = currentNode.gCost + getDistance(currentNode, neighbour);
				if(newMovementCostToNeighbour < neighbour.gCost || !openSet.contains(neighbour)) {
					neighbour.gCost = newMovementCostToNeighbour;
					neighbour.hCost = getDistance(neighbour, targetNode);
					neighbour.parent = currentNode;
					
					if(!openSet.contains(neighbour)) {
						openSet.add(neighbour);
					}
				}
			}
		}
	}
	
	static LinkedList<Node> RetracePath(Node startNode, Node endNode) {
		LinkedList<Node> path = new LinkedList<Node>();
		Node currentNode = endNode;
		
		while(currentNode != startNode) {
			path.add(currentNode);
			currentNode = currentNode.parent;
		}
		LinkedList<Node> reversedList = new LinkedList<Node>();
		for(int i = path.size() - 1; i >= 0; i--)
			reversedList.add(path.get(i));
		
		return reversedList;
	}
	
	public static int getDistance(Node node1, Node node2) {
		int dstX = Math.abs(node1.gridX - node2.gridX);
		int dstY = Math.abs(node1.gridY - node2.gridY);
		if(dstX > dstY)
			return 14 * dstY + 10 * (dstX - dstY);
		return 14 * dstX + 10 * (dstY - dstX);
	}
}