package com.prototype.src.game.ai.pathfinding;

public class Node implements IHeapItem {
	public Node(int gridX, int gridY) {
		this.gridX = gridX;
		this.gridY = gridY;
	}
	
	public int gridX;
	public int gridY;
	
	public int gCost = 0;
	public int hCost = 0;
	
	public int getFCost() {
		return gCost + hCost;
	}
	
	public Node parent;

	int compareInt(int a, int b) {
		if(a < b) return -1;
		if(a > b) return 1;
		return 0;
	}
	
	int heapIndex;
	public int compareTo(Object o) {
		Node node = (Node)o;
		int compare = compareInt(getFCost(), node.getFCost());
		if(compare == 0)
			compare = compareInt(hCost, node.hCost);
		return -compare;
	}

	public int getHeapIndex() {
		return heapIndex;
	}

	public void setHeapIndex(int index) {
		heapIndex = index;
	}
}