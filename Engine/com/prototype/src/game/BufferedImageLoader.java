package com.prototype.src.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

public class BufferedImageLoader {
	public static BufferedImage Error = loadErrorImage();
	
	public static BufferedImage loadImage(String path) {
		try {
			return ImageIO.read(Game.class.getResource(path));
		} catch(Exception e) {
			return Error;
		}
	}
	
	public static BufferedImage loadImage(String path, int width, int height) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics graphics = image.createGraphics();
		graphics.clearRect(0, 0, width, height);
		graphics.drawImage(loadImage(path), 0, 0, width, height, null);
		return image;
	}
	
	
	private static BufferedImage loadErrorImage() {
		try {
			return loadImage("/resources/error.png");
		} catch(Exception e) {
			return null;
		}
	}
}