package com.prototype.src.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.prototype.src.game.math.Point;

public class Obstacle extends GameObject {
	// Need move this to controller logic
	public Obstacle(Point point, double offsetX, double offsetY) {
		super(point, offsetX, offsetY);
		image = new BufferedImage(50, 50, BufferedImage.TYPE_INT_ARGB);
		Graphics gr = image.createGraphics();
		gr.setColor(Color.WHITE);
		gr.fillOval(0, 0, 25, 25);
		gr.drawImage(image, 0, 0, null);
	}
}