package com.prototype.src.game;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JFrame;

import com.prototype.src.game.ai.pathfinding.PathFinder;
import com.prototype.src.game.math.Point;

public class Game extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;
	public static final int Width = 1280;
	public static final int Height = 720;
	public static final String Title = "Prototype";
	
	private boolean running;
	private Thread thread;
	private Player player;
	private ArrayList<String> pressedKeys;
	private int tempFPS = 0;
	private Controller controller;
	private Grid grid = new Grid();
	private Tile grass = new Tile("/resources/grass.jpg", new Point(0, 0));
	///TEMP
	private Obstacle obstacle = new Obstacle(new Point(300, 300), 25, 25);
	///
	
	BufferedImage image = new BufferedImage(Width, Height, BufferedImage.TYPE_INT_BGR);
	
	public void run() {
		initialize();
		long lastTime = System.nanoTime();
		final double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		int updates = 0;
		int frames = 0;
		long timer = System.currentTimeMillis();
		
		while(running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			if(delta >= 1) {
				tick();
				updates++;
				delta--;
			}
			render();
			frames++;
			
			if(System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				System.out.println(updates + " Ticks, Fps " + frames);
				tempFPS = frames;
				updates = 0;
				frames = 0;
			}
		}
		stop();
	}
	
	public static void StartGame() {
		Game game = new Game();
		Dimension dimension = new Dimension(Width, Height);
		game.setPreferredSize(dimension);
		game.setMinimumSize(dimension);
		game.setMaximumSize(dimension);
		JFrame frame = new JFrame(Game.Title);
		frame.add(game);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		game.start();
	}
	
	synchronized void start() {
		if(running)
			return;
		
		
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	synchronized void stop() {
		if(!running)
			return;
		
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.exit(1);
	}
	
	void tick() {
		player.tick();
		controller.tick();
	}
	
	void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if(bs == null) {
			createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		this.grass.render(g);
		this.grid.render(g);
		player.render(g);
		player.getCrosshair().render(g);
		controller.render(g);
		g.setColor(Color.WHITE);
		g.drawString("Player.x = " + player.getPoint().getX(), 20, 30);
		g.drawString("Player.y = " + player.getPoint().getY(), 20, 50);
		g.drawString("FPS: " + this.tempFPS, 20, 70);
		g.drawString("Crosshair.x = " + player.getCrosshair().getPoint().getX(), 20, 90);
		g.drawString("Crosshair.y = " + player.getCrosshair().getPoint().getY(), 20, 110);
		g.drawString("Grid size: = " + grid.getCellWidth() + " " + grid.getCellHeight(), 20, 150);
		obstacle.render(g);
		//g.drawLine((int)(player.getX() + 16), (int)(player.getY() + 16), (int)crosshair.getX(), (int)crosshair.getY());
		
		
		g.dispose();
		bs.show();
	}
	Crosshair crosshair;
	public void initialize() {
		this.requestFocus();
		this.addKeyListener(new KeyInput(this));
		this.pressedKeys = new ArrayList<String>();
		this.player = new Player(new Point(200, 200), this, new Crosshair(new Point(0, 0), this));
		this.addMouseMotionListener(new MouseMotionInput(this));
		this.addMouseListener(new MouseInput(this));
		this.controller = new Controller(this);
		this.grid.setupSettings(5, 5);
		PathFinder.InitializeNodeArray();
	}
	
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_LEFT && !pressedKeys.contains(Integer.toString(KeyEvent.VK_LEFT))) {
			pressedKeys.add(Integer.toString(KeyEvent.VK_LEFT));
			player.setVelX(-5);
		} else if(key == KeyEvent.VK_UP && !pressedKeys.contains(Integer.toString(KeyEvent.VK_UP))) {
			pressedKeys.add(Integer.toString(KeyEvent.VK_UP));
			player.setVelY(-5);
		} else if(key == KeyEvent.VK_RIGHT && !pressedKeys.contains(Integer.toString(KeyEvent.VK_RIGHT))) {
			pressedKeys.add(Integer.toString(KeyEvent.VK_RIGHT));
			player.setVelX(5);
		} else if(key == KeyEvent.VK_DOWN && !pressedKeys.contains(Integer.toString(KeyEvent.VK_DOWN))) {
			pressedKeys.add(Integer.toString(KeyEvent.VK_DOWN));
			player.setVelY(5);
		}
	}
	
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_LEFT) {
			if(pressedKeys.contains(Integer.toString(KeyEvent.VK_RIGHT)))
				player.setVelX(5);
			else
				player.setVelX(0);
			pressedKeys.remove(Integer.toString(KeyEvent.VK_LEFT));
		} else if(key == KeyEvent.VK_UP) {
			if(pressedKeys.contains(Integer.toString(KeyEvent.VK_DOWN)))
				player.setVelY(5);
			else
				player.setVelY(0);
			pressedKeys.remove(Integer.toString(KeyEvent.VK_UP));
		} else if(key == KeyEvent.VK_RIGHT) {
			if(pressedKeys.contains(Integer.toString(KeyEvent.VK_LEFT)))
				player.setVelX(-5);
			else
				player.setVelX(0);
			pressedKeys.remove(Integer.toString(KeyEvent.VK_RIGHT));
		} else if(key == KeyEvent.VK_DOWN) {
			if(pressedKeys.contains(Integer.toString(KeyEvent.VK_UP)))
				player.setVelY(-5);
			else
				player.setVelY(0);
			pressedKeys.remove(Integer.toString(KeyEvent.VK_DOWN));
		}
		if(key == KeyEvent.VK_F) {
			this.controller.addEnemy(new Enemy(new Point(Math.random() * Game.Width, Math.random() * Game.Height), this, 16, 16, player));
		}
		if(key == KeyEvent.VK_P) {
			this.grid.setupSettings(this.grid.getCellWidth() + 5, this.grid.getCellHeight() + 5);
			this.grass.setupSettings(this.grass.getCellWidth() + 5, this.grass.getCellHeight() + 5);
		}
		if(key == KeyEvent.VK_M) {
			this.grid.setupSettings(this.grid.getCellWidth() - 5, this.grid.getCellHeight() - 5);
			this.grass.setupSettings(this.grass.getCellWidth() - 5, this.grass.getCellHeight() - 5);
		}
	}
	
	public void mouseMoved(MouseEvent e) {
		player.getCrosshair().getPoint().setX(e.getX());
		player.getCrosshair().getPoint().setY(e.getY());
	}
	
	public void mouseClicked(MouseEvent e) {
		Bullet newBullet = new Bullet(new Point(player.getPoint()), new Point(player.getCrosshair().point), this);
		this.controller.addBullet(newBullet);
	}
	
	//TEMP
	public Controller getController() {
		return this.controller;
	}
	//
	
	public void mousePressed(MouseEvent arg0) {
		this.player.setShooting(true);
	}
	
	public void mouseReleased(MouseEvent arg0) {
		this.player.setShooting(false);
	}
}