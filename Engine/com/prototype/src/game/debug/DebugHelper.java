package com.prototype.src.game.debug;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.prototype.src.game.math.Point;

public class DebugHelper {
	public static void showCoordsObject(Graphics graphics, Point point, double offsetX, double offsetY) {
		Font font = graphics.getFont();
		Color color = graphics.getColor();
		graphics.setFont(new Font("Monospaced", Font.PLAIN, 12));
		graphics.setColor(Color.WHITE);
		graphics.drawString("X: " + point.getX(), (int)(point.getX() - offsetX), (int)(point.getY() - offsetY - 15));
		graphics.drawString("Y: " + point.getY(), (int)(point.getX() - offsetX), (int)(point.getY() - offsetY - 5));
		graphics.setFont(font);
		graphics.setColor(color);
	}
}