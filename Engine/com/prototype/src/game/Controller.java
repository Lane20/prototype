package com.prototype.src.game;

import java.awt.Graphics;
import java.util.LinkedList;

public class Controller {
	public Controller(Game game) {
		//GAME FOR SPRITE?
		this.bullets = new LinkedList<Bullet>();
		this.enemies = new LinkedList<Enemy>();
	}
	
	private LinkedList<Bullet> bullets;
	private Bullet bullet;
	private LinkedList<Enemy> enemies;
	private Enemy enemy;
	
	public void addBullet(Bullet bullet) {
		this.bullets.add(bullet);
	}
	
	public void addEnemy(Enemy enemy) {
		this.enemies.add(enemy);
	}
	
	public void removeBullet(Bullet bullet) {
		this.bullets.remove(bullet);
	}
	
	public void removeEnemy(Enemy enemy) {
		this.enemies.remove(enemy);
	}
	
	public void render(Graphics g) {
		bulletRender(g);
		enemyRender(g);
	}
	
	public void tick() {
		bulletTick();
		enemyTick();
	}
	
	void bulletRender(Graphics g) {
		int countBullets = bullets.size();
		for(int i = 0; i < countBullets; i++) {
			this.bullet = bullets.get(i);
			this.bullet.render(g);
		}
	}
	
	void enemyRender(Graphics g) {
		int countEnemies = enemies.size();
		for(int i = 0; i < countEnemies; i++) {
			this.enemy = enemies.get(i);
			this.enemy.render(g);
		}
	}
	
	void bulletTick() {
		for(int i = 0; i < bullets.size(); i++) {
			this.bullet = bullets.get(i);
			if(this.bullet.getPoint().getX() >= Game.Width + 20 || this.bullet.getPoint().getX() <= -20)
				removeBullet(this.bullet);
			else if(this.bullet.getPoint().getY() >= Game.Height + 20 || this.bullet.getPoint().getY() <= -20)
				removeBullet(this.bullet);
			this.bullet.tick();
		}
	}
	
	void enemyTick() {
		for(int i = 0; i < enemies.size(); i++) {
			this.enemy = enemies.get(i);
			if(this.enemy.getPoint().getX() >= Game.Width + 20 || this.enemy.getPoint().getX() <= -20)
				removeEnemy(this.enemy);
			else if(this.enemy.getPoint().getY() >= Game.Height + 20 || this.enemy.getPoint().getY() <= -20)
				removeEnemy(this.enemy);
			this.enemy.tick();
		}
	}
}