package com.prototype.src.game;

import java.util.LinkedList;

public class Physics {
	public static boolean Collision(GameObject objA, GameObject objB) {
		return objA.getBounds().intersects(objB.getBounds());
	}
	
	public static boolean Collision(GameObject objA, LinkedList<? extends GameObject> listObjB) {
		int count = listObjB.size();
		for(int i = 0; i < count; i++)
			if(Collision(objA, listObjB.get(i)))
				return true;
		return false;
	}
}