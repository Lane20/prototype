package com.prototype.src.game;

import java.awt.Graphics;

import com.prototype.src.game.math.Point;

public class Tile extends GameObject {
	public Tile(String path, Point point) {
		super(point, 0, 0);
	    this.path = path;
	    this.setupSettings(32, 32);
	}
	
	private int cellWidth;
	private int cellHeight;
	// Temp. path => dynamic enum value
	private String path;
	
	public void setupSettings(int cellWidth, int cellHeight) {
		this.cellWidth = cellWidth;
		this.cellHeight = cellHeight;
		this.image = BufferedImageLoader.loadImage(this.path, cellWidth, cellHeight);
	}
	
	public int getCellWidth() { return this.cellWidth; }
	public int getCellHeight() { return this.cellHeight; }
	public void render(Graphics graphics) { graphics.drawImage(this.image, (int)this.point.getX(), (int)this.point.getY(), null); }
}