package com.prototype.src.game;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Grid extends RenderedObject {
	public Grid() {
		this.image = new BufferedImage(Game.Width, Game.Height, BufferedImage.TYPE_INT_ARGB);
		this.setupSettings(32, 32);
	}
	
	private int cellWidth;
	private int cellHeight;
	
	public void setupSettings(int cellWidth, int cellHeight) {
		this.cellWidth = cellWidth < 1 ? 32 : cellWidth;
		this.cellHeight = cellHeight < 1 ? 32 : cellHeight;
		Graphics2D graphics = image.createGraphics();
		graphics.setComposite(AlphaComposite.Clear);
		graphics.fillRect(0, 0, Game.Width, Game.Height);
		graphics.setComposite(AlphaComposite.Src);
		graphics.setColor(Color.GRAY);
		int currentX = 0;
		while((currentX += this.cellWidth) < Game.Width)
			graphics.drawLine(currentX, 0, currentX, Game.Height);
		int currentY = 0;
		while((currentY += this.cellHeight) < Game.Height)
			graphics.drawLine(0, currentY, Game.Width, currentY);
		graphics.drawImage(image, 0, 0, null);
	}
	
	public int getCellWidth() { return this.cellWidth; }
	public int getCellHeight() { return this.cellHeight; }
	public void render(Graphics graphics) { graphics.drawImage(this.image, 0, 0, null); }
}